using Statistics
using DataStructures

const kB = 1.38064852e-23 #J/K
const ρ = 2.33*1e-3/((1e-2)^3) # kg/m^3
const m0 = 9.109e-31 # kg
const eV = 1.602e-19 # J
const q= eV
const ml = 0.98*m0 # Kg
const mt = 0.19*m0 # kg
const D_ac = 9*eV # J
const α = 0.5 / eV # J^-1
const vsound = 9e5*1e-2 # m/s
const T_op_g = [140, 215, 720] # K
const T_op_f = [220, 550, 685]  # K
const DtK_g =  eV*1e8/1e-2 * [0.5, 0.8, 11] # J/m
const DtK_f =  eV*1e8/1e-2 * [0.3, 2, 2] # J/m
const meff = 3/(1/ml + 2/mt) #kg
const md = (ml*mt^2)^(1/3) # kg
const ħ = 1.054e-34 # J-s


##### Utility Functions 

# Calculuates acoustic scattering given energy and temperature
acoustic_scattering(E, T) = (@fastmath (2^0.5 * md^1.5 * kB*T * D_ac^2 * (E + α*E^2)^0.5 * (1 + 2*α*E) / (π * ħ^4 * vsound^2 * ρ)), E)

# Calculuates optical scattering given energy and temperature, as well as
# scattering paramets that determine the type of scattering
function optical_scattering(E, T_op, DtK, T; absorption=true, g_mechanism=true)
    Z = g_mechanism ? 1 : 4
    E_op = kB*T_op
    newE = E + ( absorption ? E_op : -E_op)
    @fastmath if newE > 0
        E = newE
        N_op = (exp(T_op/T) - 1)^-1
        S_op = (DtK)^2*md^1.5*Z * (E + α*E^2 + 0im)^0.5 * (1 + 2*α*E) /( (√2)*π*ρ*ħ^2 * E_op)
        S_op *= ( absorption ? N_op : N_op + 1 )
    else
        S_op = 0
    end
    return (S_op, E)
end

# Calculates all scattering rates and energies for all 13 scattering types
function calculate_scattering_rates(E, T)
	rates = Array{Float64}(undef, 13)
	newEs = Array{Float64}(undef, 13)
    @simd for n=1:3
        @inbounds begin
            rates[n],   newEs[n]   = optical_scattering(E, T_op_g[n], DtK_g[n], T, absorption=true,  g_mechanism=true)
        rates[n+3], newEs[n+3] = optical_scattering(E, T_op_f[n], DtK_f[n], T, absorption=true,  g_mechanism=false)
        rates[n+6], newEs[n+6] = optical_scattering(E, T_op_g[n], DtK_g[n], T, absorption=false, g_mechanism=true)
        rates[n+9], newEs[n+9] = optical_scattering(E, T_op_f[n], DtK_f[n], T, absorption=false, g_mechanism=false)
            end
    end

    rates[end], newEs[end] = acoustic_scattering(E, T)
    return (rates=rates, newEs=newEs)
end

# Calculates the total scattering at 2eV, which helps determine virtual
# scattering
calc_total_scatter(T; E=2*eV) = sum(calculate_scattering_rates(E, T).rates)

# returns a vector of size 3 with kx, ky, kz
function k_vector(E)
    k = ((E*(1+ α*E) * 2 * meff)^0.5 )/ ħ
    r = rand(Float64, 2)
    ϕ = 2π*r[1]
    θ = acos(1-2*r[2])
    return @fastmath [k*sin(θ)*cos(ϕ), k*sin(θ)*sin(ϕ), k*cos(θ)]
end
    
# returns velocity in direction of electric field
# dotting in direction of Field, [111]
velocity(E, k) = @fastmath (sum(k) *  ħ / (2*meff*(1+2*α*E)) * 3^-0.5 )

####### Monte Carlo Specific Functions and structs
#
# A single sample that keeps track of energy and velocity
struct sample
	E::Float64
	v::Float64
#	time::Float64
end

# A struct that contains all the parameters for a given monte carlo run, as
# well as storing a linked list of sample points
mutable struct montecarlo
	samples::Cons{Any} # LinkedList of samples
	E::Float64
	k::Array{Float64, 1}
	v::Float64
	field::Float64
	time::Float64
	dt::Float64
	max_time::Float64
	T::Float64
	total_scatter::Float64
end

# returns a randomly chosen energy based off of the binning method
# discussed in the powerpoint and in class
# -1 is returned for virtual scattering
function choose_scatter(mc::montecarlo)
    (rates, newEs) = calculate_scattering_rates(mc.E, mc.T)
	bins = cumsum(rates[1:13]) ./ mc.total_scatter
    r = rand(Float64)
    @inbounds for n=1:13
        if r < bins[n]
           return newEs[n]
        end
    end
	return -1 
end

# Randomly generates a flight time based using the method discussed in class
# Accounts for virtual scattering
gen_flight_time(mc::montecarlo) = -log(rand(Float64)) / mc.total_scatter

# Modifies E and k due to scattering and samples E and v and adds the values to the samples
# linked list in the monte carlo struct
function scatter!(mc::montecarlo)
	E = choose_scatter(mc)
	if E != -1
		mc.E = E
		mc.k = k_vector(E)
		mc.v = velocity(E, mc.k)
	end
	mc.samples = Cons(sample(mc.E, mc.v), mc.samples)
end

# modifies E, k, and turns the clock for the duration of the drift
# samples for each duration dt
function drift!(mc::montecarlo)
    flight_time = gen_flight_time(mc)
    flight_num = convert(Int64, round(flight_time/mc.dt))
    mc.time += flight_num*mc.dt
    @fastmath @inbounds for i=1:flight_num
		mc.k .-= (q*mc.field*mc.dt / ħ) * 3^-.5 * ones(3)
		mc.E = ((1+4α*(ħ^2*sum(mc.k.^2)/(2*meff)))^0.5 - 1 ) / (2*α)
		mc.v = velocity(mc.E, mc.k)
		mc.samples = Cons(sample(mc.E, mc.v), mc.samples)
    end
end

# dynamically change E_max and thus virtual scattering as a function
# of field
# Simple linear function of field
calc_E_max(field) = 1.45e-26*field + 0.0.0.0.0.5*eV

# initializes the montecarlo struct
function montecarlo_init(field, max_time, dt, T)
    E = 1.5*kB*T
	k = k_vector(E)
	v = velocity(E, k)
	samples = Cons(sample(E, v), nil())
	time = 0
    E_max = calc_E_max(field)
	total_scatter = calc_total_scatter(T; E=E_max)
	return montecarlo(samples, E, k, v, field, time, dt, max_time, T, total_scatter)
end

# Runs the monte carlo simulation given field, max_time, dt, and temperature, 
# returns a linked list of samples
function run_montecarlo(field, max_time, dt, T)
	mc = montecarlo_init(field, max_time, dt, T)
    while mc.time < mc.max_time
        drift!(mc)
        scatter!(mc)
     end
    return mc.samples
end


